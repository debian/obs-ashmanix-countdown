Source: obs-ashmanix-countdown
Section: video
Priority: optional
Maintainer: Joao Eriberto Mota Filho <eriberto@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), cmake, libobs-dev (>= 29), qt6-base-dev
Standards-Version: 4.7.0
Homepage: https://obsproject.com/forum/resources/ashmanix-countdown-timer.1610/
Vcs-Browser: https://salsa.debian.org/debian/obs-ashmanix-countdown
Vcs-Git: https://salsa.debian.org/debian/obs-ashmanix-countdown.git

Package: obs-ashmanix-countdown
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, obs-studio (>= 29)
Enhances: obs-studio
Breaks: obs-text-slideshow (<= 1.5.2-3)
Description: plugin for OBS Studio to create a countdown timer
 This plugin lets to use a text source in OBS to show a countdown timer that
 updates in real time. This is useful to show to viewers the time left before
 a transmission.
 .
 The plugin appears as a dockable widget and comes with options to display a
 final message or to switch to a scene when the counter reaches zero. These
 two features can be disabled via checkbox, so the countdown will show the
 zero time when waiting for someone to switch the scene.
 .
 When configuring, is possible to set a period of time or a real time to
 count down from it. Also is possible to choose not show some part of the
 time (hours, minutes, seconds).
